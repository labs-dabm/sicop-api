# Sistema de Gerenciamento de Contratos e Processos - SICOP

## Propósito:

O propósito do SISCOP-API é aplicar o conhecimento adquirido das novas tecnologias utilizadas na Modernização do SINGRA.

## Tecnologias Utilizadas no Projeto

- **JAVA 11+** - <https://jdk.java.net/archive/>
- **Maven** - <https://maven.apache.org/>
- **PostgreSQL ver.: 9.4.21** - <https://www.postgresql.org/download/>

## Pré-requisitos
- OpenJDK 11+ instalado;
- Banco de Dados PostgresSQL criado.

### Criar BD no PostgreSQL:

```bash
# coloque o executavel do posgresql no PATH do SO.
# para o Windows
sysdm.cpl
# abrir aba: avançados -> variavel de ambiente
'C:\desenv\PostgreSQL\9.4\bin'
# para o Linux desconsiderar acoes acima
#
# logar no postgresql
psql -h localhost -U postgres -d postgres -p 5432
psql (9.4.21)
AVISO: página de código do Console (850) difere da página de código do Windows (1252)
         caracteres de 8 bits podem não funcionar corretamente. Veja página de
         referência do psql "Notes for Windows users" para obter detalhes.
Digite "help" para ajuda.

# -- cria BD 
postgres=$ create role sicop createdb createrole inherit login encrypted password 'sicop';
CREATE ROLE
# criar usuário e senha
postgres=$ create database sicop owner sicop;
CREATE DATABASE
postgres=$ \q
```

### Recursos disponíveis no sistema:

#### Para retornar registros cadastrados no BD.

```bash
# retorna todos os usuários cadastrados no banco de dados
GET: http://localhost:8080/usuarios
# retornar usuário de ID 1 cadastrado no BD
GET: http://localhost:8080/usuarios/1
```

#### Para inserir registro no BD.

```bash
# retorna todos os usuários cadastrados no banco de dados
POST: http://localhost:8080/usuarios
# headers
Content-Type = application/json
```

JSON com dados mínimos

```json
{
    "login": "nome-usuario",
    "senha": "senha",
    "confirmaSenha": "senha",
    "nomeGuerra": "Anders",
    "nip": "XX.XXXX.XX",
}
```

JSON com dados mais completos com PG e Corpo/Quadro:

```json
{
    "login": "nome-usuario",
    "senha": "senha",
    "confirmaSenha": "senha",
    "nomeCompleto": "Carlos Alberto Silva Anders",
    "nomeGuerra": "Anders",
    "nip": "XX.XXXX.XX",
    "corpoQuadro": {
        "id": 11
    },
    "postoGraduacao": {
        "id": 10
    },
    "email": "seu-email@servidor",
    "ativo": true,
    "dataCadastro": "2019-07-07"
}
```



#### Para atualizar registro no BD.

```bash
# retorna todos os usuários cadastrados no banco de dados
PUT: http://localhost:8080/usuarios
# headers
Content-Type = application/json
```

JSON:

```json
# destaca-se que ao atualizar os dados mínimos os dados que já
# existem preenchidos que não estão contemplados no JSON serão 
# alterados para o valor null, comportamento default do JSON.
{
    "login": "nome-usuario",
    "senha": "senha",
    "confirmaSenha": "senha",
    "nomeGuerra": "Anders",
    "nip": "XX.XXXX.XX",
}
```

#### Para remover registro no BD.

```bash
# remover o usuário de ID 1 cadastrado no BD
DELETE: http://localhost:8080/usuarios/1
```

## Clonando o Projeto

### Repositório do [https://gitlab.com/labs-dabm/sicop-api](https://gitlab.com/labs-dabm/sicop-api)

#### Comandos do Git

```bash
# clonando o projeto
git clone git@gitlab.com:labs-dabm/sicop-ui.git
# ou se desejar baixar para outra pasta
git clone git@gitlab.com:labs-dabm/sicop-ui.git sicop
# se não possuir a chave SSH na conta tem baixar via HTTPS
git clone https://gitlab.com/labs-dabm/sicop-ui.git
```

#### Configurar o Git:

```bash
# configurar nome e email no nível global do sistema para identificar no commit
git config --global user.name "Seu Nome"
git config --global user.email "email@provedor.com"
```

#### Criando um chave SSH Para criar uma chave privada/publica no git:]

Para cadastrar uma chave privado no seu computador proceda conforme comandos abaixo. Após será necessário copiar a chave pública e disponibilizá-la em seu profile na conta criada no [Git Lab](https://gitlab.com/profile/keys).

```bash
$ git ssh-keygen -t rsa -C "seuemails@provedor.com"
# Para listar a chave pública
$ cat ~/.ssh/id_rsa.pub 
# ou se preferir pelo editor de texto padrão ou cacadastrado
$ subl ~/.ssh/id_rsa.pub
```

#### Iniciando o Git Flow no projeto:

```bash
# verficar os branches trazidos do repositório com os commits
$ git branch -a –v
# criando e alternando para o branch develop – usado como referência de desenv.
$ git checkout -b develop origin/develop
# Iniciando de fato o git flow no projeto clonado. Nessa etapa deixa com as opções default
$ git flow init
```

### Gerando o jar executável do projeto

Para gerar o **jar** executavel, basta adicionar na tag build do `pom.xml` o seguinte:

```xml
...
<build>
		<plugins>
			<plugin>
				<groupId>org.springframework.boot</groupId>
				<artifactId>spring-boot-maven-plugin</artifactId>
			</plugin>
			<!-- DEFINE O PLUGIN MAVEN RESPONSÁVEL POR COMPILAR O PROJETO 
                SUA VERSÃO E O JDK UTILIZADO -->
            <plugin>
                <groupId>org.apache.maven.plugins</groupId>
                <artifactId>maven-compiler-plugin</artifactId>
                <configuration>
                    <archive>
                        <!-- DEFINE QUAL É A CLASSE MAIN DA APLICAÇÃO -->
                        <manifest>
                            <addClasspath>true</addClasspath>
                            <mainClass>br.mil.marinha.dabm.sicop.SicopApplication</mainClass>
                        </manifest>
                    </archive>
                </configuration>
            </plugin>
		</plugins>
	</build>
```

Executar o projeto pelo **bash**, desde que se tenha instalado no PATH o maven corretamente:

```bash
# limpar
$ mvn clean 
# gerar o pacote
$ mvn package 

# para rodar o jar basta:
java -jar target\sicop-0.0.1-SNAPSHOT.jar
# ou caso queira alterar algum parametro do resource
# ex.: alterar usuario/senha banco
java -jar target\sicop-0.0.1-SNAPSHOT.jar --spring.datasource.username=sicop --spring.datasource.password=sicop
```