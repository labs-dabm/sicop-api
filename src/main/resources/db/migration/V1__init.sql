-- Adicionar Corpo/Quadro
--ALTER TABLE corpo_quadro ALTER COLUMN id SET DEFAULT nextval ('corpo_quadro_seq'::regclass);
insert into corpo_quadro (id, nome, sigla, ordem) values (1, '','',1);
insert into corpo_quadro (id, nome, sigla, ordem) values (2, 'Quadro de Oficiais da Armada','CA',3);
insert into corpo_quadro (id, nome, sigla, ordem) values (3, 'Quadro Complementar de Oficiais da Armada','QC-CA',5);
insert into corpo_quadro (id, nome, sigla, ordem) values (4, 'Quadro de Oficiais Fuzileiros Navais','FN',7);
insert into corpo_quadro (id, nome, sigla, ordem) values (5, 'Quadro Complementar de Oficiais Fuzileiros Navais','QC-FN',9);
insert into corpo_quadro (id, nome, sigla, ordem) values (6, 'Quadro de Oficiais Intendentes da Marinha','IM',11);
insert into corpo_quadro (id, nome, sigla, ordem) values (7, 'Quadro Complementar de Oficiais Intendentes da Marinha','QC-IM',13);
insert into corpo_quadro (id, nome, sigla, ordem) values (8, 'Quadro de Médicos','Md',15);
insert into corpo_quadro (id, nome, sigla, ordem) values (9, 'Quadro de Cirurgiões-Dentistas','CD',17);
insert into corpo_quadro (id, nome, sigla, ordem) values (10, 'Quadro de Apoio à Saúde','S',19);
insert into corpo_quadro (id, nome, sigla, ordem) values (11, 'Quadro Técnico','T',21);
insert into corpo_quadro (id, nome, sigla, ordem) values (12, 'Quadro de Capelães Navais','CN',23);
insert into corpo_quadro (id, nome, sigla, ordem) values (13, 'Quadro Auxiliar da Armada','AA',25);
insert into corpo_quadro (id, nome, sigla, ordem) values (14, 'Quadro Auxiliar de Fuzileiros Navais','AFN',27);
insert into corpo_quadro (id, nome, sigla, ordem) values (15, 'Corpo de Engenheiros da Marinha','EN',29);
insert into corpo_quadro (id, nome, sigla, ordem) values (16, 'Quadro de Praças da Armada','QPA',41);
insert into corpo_quadro (id, nome, sigla, ordem) values (17, 'Quadro Especial de Praças da Armada','QEPA',43);
insert into corpo_quadro (id, nome, sigla, ordem) values (18, 'Quadro de Praças de Fuzileiros Navais','QPFN',45);
insert into corpo_quadro (id, nome, sigla, ordem) values (19, 'Quadro de Músicos','QMU',47);
insert into corpo_quadro (id, nome, sigla, ordem) values (20, 'Quadro Especial de Fuzileiros Navais','QEFN',49);
insert into corpo_quadro (id, nome, sigla, ordem) values (21, 'Quadro Auxiliar de Praças','QAP',51);
insert into corpo_quadro (id, nome, sigla, ordem) values (22, 'Quadro Auxiliar Técnico de Praças','QATP',53);
insert into corpo_quadro (id, nome, sigla, ordem) values (23, 'Quadro Técnico de Praças','QTP',55);
insert into corpo_quadro (id, nome, sigla, ordem) values (24, 'Quadro Especial Auxiliar de Praças','QEAP',57);

--Adicionar Posto/Graduação
--ALTER TABLE posto_graduacao ALTER COLUMN id SET DEFAULT nextval ('posto_graduacao_seq'::regclass);
 insert into posto_graduacao (id, nome, sigla, ordem) values (1, '','',1);  
 insert into posto_graduacao (id, nome, sigla, ordem) values (2, 'Almirante','Alte',2);  
 insert into posto_graduacao (id, nome, sigla, ordem) values (3, 'Almirante de Esquadra','V Alte',3); 
 insert into posto_graduacao (id, nome, sigla, ordem) values (4, 'Vice-Almirante','V Alte',4); 
 insert into posto_graduacao (id, nome, sigla, ordem) values (5, 'Contra-Almirante','C Alte',5); 
 insert into posto_graduacao (id, nome, sigla, ordem) values (6, 'Capitão de Mar e Guerra','CMG',6);  
 insert into posto_graduacao (id, nome, sigla, ordem) values (7, 'Capitão de Fragata','CF',7);  
 insert into posto_graduacao (id, nome, sigla, ordem) values (8, 'Capitão de Corveta','CC',8);  
 insert into posto_graduacao (id, nome, sigla, ordem) values (9, 'Capitão-Tenente','CT',9);  
 insert into posto_graduacao (id, nome, sigla, ordem) values (10, 'Primeiro-Tenente','1º Ten',10);
 insert into posto_graduacao (id, nome, sigla, ordem) values (11, 'Segundo-Tenente','2º Ten',11);
 insert into posto_graduacao (id, nome, sigla, ordem) values (12, 'Guarda-Marinha','GM',12);  
 insert into posto_graduacao (id, nome, sigla, ordem) values (13, 'Suboficial','SO',21);  
 insert into posto_graduacao (id, nome, sigla, ordem) values (14, 'Primeiro-Sargento','1º SG',22);
 insert into posto_graduacao (id, nome, sigla, ordem) values (15, 'Segundo-Sargento','2º SG',23);
 insert into posto_graduacao (id, nome, sigla, ordem) values (16, 'Terceiro-Sargento','3º SG',24);
 insert into posto_graduacao (id, nome, sigla, ordem) values (17, 'Cabo','CB',25);  
 insert into posto_graduacao (id, nome, sigla, ordem) values (18, 'Soldado','SD',26);  
 insert into posto_graduacao (id, nome, sigla, ordem) values (19, 'Marinheiro','MN',27);  
 insert into posto_graduacao (id, nome, sigla, ordem) values (20, 'Aluno','AL',28);  
 insert into posto_graduacao (id, nome, sigla, ordem) values (21, 'Recruta','RC',29);  
 insert into posto_graduacao (id, nome, sigla, ordem) values (22, 'Grumete','GR',30);  
 insert into posto_graduacao (id, nome, sigla, ordem) values (23, 'Aprendiz-marinheiro','AM',31);  
 insert into posto_graduacao (id, nome, sigla, ordem) values (24, 'Marinheiro-Recruta','MN-RC',32); 

-- Adicionar Especialidades
--ALTER TABLE especialidade ALTER COLUMN id SET DEFAULT nextval ('especialidade_seq'::regclass);
-- Quadro Técnico
insert into especialidade (id, nome, sigla, corpo_quadro_id) values (1, '','',1);
insert into especialidade (id, nome, sigla, corpo_quadro_id) values (2, 'Administração','',11);
insert into especialidade (id, nome, sigla, corpo_quadro_id) values (3, 'Biblioteconomia','',11);
insert into especialidade (id, nome, sigla, corpo_quadro_id) values (4, 'Ciências Contábeis','',11);
insert into especialidade (id, nome, sigla, corpo_quadro_id) values (5, 'Ciências Econômicas','',11);
insert into especialidade (id, nome, sigla, corpo_quadro_id) values (6, 'Comunicação Social','',11);
insert into especialidade (id, nome, sigla, corpo_quadro_id) values (7, 'Desenho Industrial','',11);
insert into especialidade (id, nome, sigla, corpo_quadro_id) values (8, 'Direito','',11);
insert into especialidade (id, nome, sigla, corpo_quadro_id) values (9, 'Estatística','',11);
insert into especialidade (id, nome, sigla, corpo_quadro_id) values (10, 'Física','',11);
insert into especialidade (id, nome, sigla, corpo_quadro_id) values (11, 'Geologia','',11);
insert into especialidade (id, nome, sigla, corpo_quadro_id) values (12, 'Informática','',11);
insert into especialidade (id, nome, sigla, corpo_quadro_id) values (13, 'Matemática','',11);
insert into especialidade (id, nome, sigla, corpo_quadro_id) values (14, 'Meteorologia','',11);
insert into especialidade (id, nome, sigla, corpo_quadro_id) values (15, 'Letras Português','',11);
insert into especialidade (id, nome, sigla, corpo_quadro_id) values (16, 'Oceanografia','',11);
insert into especialidade (id, nome, sigla, corpo_quadro_id) values (17, 'Pedagogia','',11);
insert into especialidade (id, nome, sigla, corpo_quadro_id) values (18, 'Psicologia','',11);
insert into especialidade (id, nome, sigla, corpo_quadro_id) values (19, 'Serviço Social','',11);
insert into especialidade (id, nome, sigla, corpo_quadro_id) values (20, 'Sergurança do Tráfego Aquaviário','',11);
-- Corpo de Engenheiros
insert into especialidade (id, nome, sigla, corpo_quadro_id) values (21, 'Arquitetura e Urbanismo','',15);
insert into especialidade (id, nome, sigla, corpo_quadro_id) values (22, 'Engenharia Aeronáutica','',15);
insert into especialidade (id, nome, sigla, corpo_quadro_id) values (23, 'Engenharia Cartográfica','',15);
insert into especialidade (id, nome, sigla, corpo_quadro_id) values (24, 'Engenharia Civil','',15);
insert into especialidade (id, nome, sigla, corpo_quadro_id) values (25, 'Engenharia de Materiais','',15);
insert into especialidade (id, nome, sigla, corpo_quadro_id) values (26, 'Engenharia de Produção','',15);
insert into especialidade (id, nome, sigla, corpo_quadro_id) values (27, 'Engenharia de Sistemas de Computação','',15);
insert into especialidade (id, nome, sigla, corpo_quadro_id) values (28, 'Engenharia de Telecomunicações','',15);
insert into especialidade (id, nome, sigla, corpo_quadro_id) values (29, 'Engenharia Elétrica','',15);
insert into especialidade (id, nome, sigla, corpo_quadro_id) values (30, 'Engenharia Eletrônica','',15);
insert into especialidade (id, nome, sigla, corpo_quadro_id) values (31, 'Engenharia Mecânica','',15);
insert into especialidade (id, nome, sigla, corpo_quadro_id) values (32, 'Engenharia Mecatrônica','',15);
insert into especialidade (id, nome, sigla, corpo_quadro_id) values (33, 'Engenharia Naval','',15);
insert into especialidade (id, nome, sigla, corpo_quadro_id) values (34, 'Engenharia Nuclear','',15);
insert into especialidade (id, nome, sigla, corpo_quadro_id) values (35, 'Engenharia Química','',15);
-- Quadro de Médicos
insert into especialidade (id, nome, sigla, corpo_quadro_id) values (36, 'Alergologia e Imunologia','',5);
insert into especialidade (id, nome, sigla, corpo_quadro_id) values (37, 'Anestesiologia','',5);
insert into especialidade (id, nome, sigla, corpo_quadro_id) values (38, 'Cancerologia/Clínica','',5);
insert into especialidade (id, nome, sigla, corpo_quadro_id) values (39, 'Cardiologia','',5);
insert into especialidade (id, nome, sigla, corpo_quadro_id) values (40, 'Cirurgia Cardíaca','',5);
insert into especialidade (id, nome, sigla, corpo_quadro_id) values (41, 'Cirurgia Geral','',5);
insert into especialidade (id, nome, sigla, corpo_quadro_id) values (42, 'Cirurgia Plástica','',5);
insert into especialidade (id, nome, sigla, corpo_quadro_id) values (43, 'Clínica Médica','',5);
insert into especialidade (id, nome, sigla, corpo_quadro_id) values (44, 'Cirurgia Torácica','',5);
insert into especialidade (id, nome, sigla, corpo_quadro_id) values (45, 'Cirurgia Vascular','',5);
insert into especialidade (id, nome, sigla, corpo_quadro_id) values (46, 'Dermatologia','',5);
insert into especialidade (id, nome, sigla, corpo_quadro_id) values (47, 'Endocrinologia','',5);
insert into especialidade (id, nome, sigla, corpo_quadro_id) values (48, 'Fisiatria/Medicina Física','',5);
insert into especialidade (id, nome, sigla, corpo_quadro_id) values (49, 'Gastroenterologia','',5);
insert into especialidade (id, nome, sigla, corpo_quadro_id) values (50, 'Geriatria','',5);
insert into especialidade (id, nome, sigla, corpo_quadro_id) values (51, 'Hematologia','',5);
insert into especialidade (id, nome, sigla, corpo_quadro_id) values (52, 'Ginecologia e Obstetrícia','',5);
insert into especialidade (id, nome, sigla, corpo_quadro_id) values (53, 'Infectologia','',5);
insert into especialidade (id, nome, sigla, corpo_quadro_id) values (54, 'Medicina Intensiva','',5);
insert into especialidade (id, nome, sigla, corpo_quadro_id) values (55, 'Medicina Legal','',5);
insert into especialidade (id, nome, sigla, corpo_quadro_id) values (56, 'Medicina Nuclear','',5);
insert into especialidade (id, nome, sigla, corpo_quadro_id) values (57, 'Nefrologia','',5);
insert into especialidade (id, nome, sigla, corpo_quadro_id) values (58, 'Neurocirurgia','',5);
insert into especialidade (id, nome, sigla, corpo_quadro_id) values (59, 'Neurologia','',5);
insert into especialidade (id, nome, sigla, corpo_quadro_id) values (60, 'Oftalmologia','',5);
insert into especialidade (id, nome, sigla, corpo_quadro_id) values (61, 'Ortopedia e Traumatologia','',5);
insert into especialidade (id, nome, sigla, corpo_quadro_id) values (62, 'Otorrinolaringologia','',5);
insert into especialidade (id, nome, sigla, corpo_quadro_id) values (63, 'Patologia','',5);
insert into especialidade (id, nome, sigla, corpo_quadro_id) values (64, 'Pediatria','',5);
insert into especialidade (id, nome, sigla, corpo_quadro_id) values (65, 'Pneumologia','',5);
insert into especialidade (id, nome, sigla, corpo_quadro_id) values (66, 'Psiquiatria','',5);
insert into especialidade (id, nome, sigla, corpo_quadro_id) values (67, 'Radiologia','',5);
insert into especialidade (id, nome, sigla, corpo_quadro_id) values (68, 'Radioterapia','',5);
insert into especialidade (id, nome, sigla, corpo_quadro_id) values (69, 'Reumatologia','',5);
insert into especialidade (id, nome, sigla, corpo_quadro_id) values (70, 'Urologia','',5);
-- Quadro de Cirurgiões-Dentistas
insert into especialidade (id, nome, sigla, corpo_quadro_id) values (71, 'Cirurgia e Traumatologia Buco-Maxilo-Facial','',9);
insert into especialidade (id, nome, sigla, corpo_quadro_id) values (72, 'Dentística','',9);
insert into especialidade (id, nome, sigla, corpo_quadro_id) values (73, 'Endodontia','',9);
insert into especialidade (id, nome, sigla, corpo_quadro_id) values (74, 'Implantodontia','',9);
insert into especialidade (id, nome, sigla, corpo_quadro_id) values (75, 'Odontopediatria','',9);
insert into especialidade (id, nome, sigla, corpo_quadro_id) values (76, 'Ortodontia','',9);
insert into especialidade (id, nome, sigla, corpo_quadro_id) values (77, 'Patologia Bucal e Estomatologia','',9);
insert into especialidade (id, nome, sigla, corpo_quadro_id) values (78, 'Periodontia','',9);
insert into especialidade (id, nome, sigla, corpo_quadro_id) values (79, 'Radiologia','',9);
-- Quadro de Apoio à Saúde
insert into especialidade (id, nome, sigla, corpo_quadro_id) values (80, 'Enfermagem','',10); 
insert into especialidade (id, nome, sigla, corpo_quadro_id) values (81, 'Farmácia','',10); 
insert into especialidade (id, nome, sigla, corpo_quadro_id) values (82, 'Fisioterapia','',10); 
insert into especialidade (id, nome, sigla, corpo_quadro_id) values (83, 'Fonoaudiologia','',10); 
insert into especialidade (id, nome, sigla, corpo_quadro_id) values (84, 'Nutrição','',10); 
insert into especialidade (id, nome, sigla, corpo_quadro_id) values (85, 'Psicologia','',10);
-- Quadro Complementar de Oficiais da Armada
insert into especialidade (id, nome, sigla, corpo_quadro_id) values (86, 'Eletrônica','',3); 
insert into especialidade (id, nome, sigla, corpo_quadro_id) values (87, 'Máquinas','',3); 
insert into especialidade (id, nome, sigla, corpo_quadro_id) values (88, 'Sistemas de Armas','',3);
-- Quadro Complementar de Oficiais Fuzileiros Navais
insert into especialidade (id, nome, sigla, corpo_quadro_id) values (89, 'Educação Física','',5);
insert into especialidade (id, nome, sigla, corpo_quadro_id) values (90, 'Eletrônica','',5); 
insert into especialidade (id, nome, sigla, corpo_quadro_id) values (91, 'Máquinas','',5); 
insert into especialidade (id, nome, sigla, corpo_quadro_id) values (92, 'Sistemas de Armas','',5);

insert into especialidade (id, nome, sigla, corpo_quadro_id) values (93, 'Armamento','AM',16);   
insert into especialidade (id, nome, sigla, corpo_quadro_id) values (94, 'Arrumador','AR',16);   
insert into especialidade (id, nome, sigla, corpo_quadro_id) values (95, 'Artífice de Mecânica','MC',16); 
insert into especialidade (id, nome, sigla, corpo_quadro_id) values (96, 'Artífice de Metalurgia','MT',16); 
insert into especialidade (id, nome, sigla, corpo_quadro_id) values (97, 'Aviação','AV',16);   
insert into especialidade (id, nome, sigla, corpo_quadro_id) values (98, 'Caldeiras','CA',16);   
insert into especialidade (id, nome, sigla, corpo_quadro_id) values (99, 'Carpintaria','CP',16);   
insert into especialidade (id, nome, sigla, corpo_quadro_id) values (100, 'Comunicações Interiores','CI',16);  
insert into especialidade (id, nome, sigla, corpo_quadro_id) values (101, 'Comunicações Navais','CN',16);  
insert into especialidade (id, nome, sigla, corpo_quadro_id) values (102, 'Cozinheiro','CO',16);   
insert into especialidade (id, nome, sigla, corpo_quadro_id) values (103, 'Direção de Tiro','DT',16); 
insert into especialidade (id, nome, sigla, corpo_quadro_id) values (104, 'Eletricidade','EL',16);   
insert into especialidade (id, nome, sigla, corpo_quadro_id) values (105, 'Eletrônica','ET',16);   
insert into especialidade (id, nome, sigla, corpo_quadro_id) values (106, 'Hidrografia e Navegação','HN',16); 
insert into especialidade (id, nome, sigla, corpo_quadro_id) values (107, 'Máquinas','MA',16);   
insert into especialidade (id, nome, sigla, corpo_quadro_id) values (108, 'Manobras e Reparos','RM',16); 
insert into especialidade (id, nome, sigla, corpo_quadro_id) values (109, 'Mergulho','MG',16);   
insert into especialidade (id, nome, sigla, corpo_quadro_id) values (110, 'Motores','MO',16);   
insert into especialidade (id, nome, sigla, corpo_quadro_id) values (111, 'Operador de Radar','OR',16); 
insert into especialidade (id, nome, sigla, corpo_quadro_id) values (112, 'Operador de Sonar','SO',16); 
insert into especialidade (id, nome, sigla, corpo_quadro_id) values (113, 'Sinais','SI',16);

insert into especialidade (id, nome, sigla, corpo_quadro_id) values (114, 'Artilharia','AT',22);   
insert into especialidade (id, nome, sigla, corpo_quadro_id) values (115, 'Aviação','AV',22);   
insert into especialidade (id, nome, sigla, corpo_quadro_id) values (116, 'Corneta-Tambor','CT',22);   
insert into especialidade (id, nome, sigla, corpo_quadro_id) values (117, 'Enfermagem','EF',22);   
insert into especialidade (id, nome, sigla, corpo_quadro_id) values (118, 'Engenharia','EG',22);   
insert into especialidade (id, nome, sigla, corpo_quadro_id) values (119, 'Escrita','ES',22);   
insert into especialidade (id, nome, sigla, corpo_quadro_id) values (120, 'Infantaria','IF',22);   
insert into especialidade (id, nome, sigla, corpo_quadro_id) values (121, 'Música','MU',22);   
insert into especialidade (id, nome, sigla, corpo_quadro_id) values (122, 'Motores e Máquinas','MO',22);

insert into especialidade (id, nome, sigla, corpo_quadro_id) values (123, 'Administração','AD',23);   
insert into especialidade (id, nome, sigla, corpo_quadro_id) values (124, 'Administração Hospitalar','AH',23);  
insert into especialidade (id, nome, sigla, corpo_quadro_id) values (125, 'Barbeiro','BA',23);   
insert into especialidade (id, nome, sigla, corpo_quadro_id) values (126, 'Contabilidade','CL',23);   
insert into especialidade (id, nome, sigla, corpo_quadro_id) values (127, 'Desenho e Arquitetura','DA',23); 
insert into especialidade (id, nome, sigla, corpo_quadro_id) values (128, 'Desenho Mecânico','DM',23);  
insert into especialidade (id, nome, sigla, corpo_quadro_id) values (129, 'Edificações','ED',23);   
insert into especialidade (id, nome, sigla, corpo_quadro_id) values (130, 'Educação Física','EP',23);  
insert into especialidade (id, nome, sigla, corpo_quadro_id) values (131, 'Eletrônica','EO',23);   
insert into especialidade (id, nome, sigla, corpo_quadro_id) values (132, 'Eletrotécnica','TE',23);   
insert into especialidade (id, nome, sigla, corpo_quadro_id) values (133, 'Estatística','AE',23);   
insert into especialidade (id, nome, sigla, corpo_quadro_id) values (134, 'Estruturas Navais','EN',23);  
insert into especialidade (id, nome, sigla, corpo_quadro_id) values (135, 'Faroleiro','FR',23);   
insert into especialidade (id, nome, sigla, corpo_quadro_id) values (136, 'Geodésia e Cartografia','GC',23); 
insert into especialidade (id, nome, sigla, corpo_quadro_id) values (137, 'Gráfica','GR',23);   
insert into especialidade (id, nome, sigla, corpo_quadro_id) values (138, 'Higiene Dental','HD',23);  
insert into especialidade (id, nome, sigla, corpo_quadro_id) values (139, 'Marcenaria','NA',23);   
insert into especialidade (id, nome, sigla, corpo_quadro_id) values (140, 'Mecânica','MI',23);   
insert into especialidade (id, nome, sigla, corpo_quadro_id) values (141, 'Metalurgia','ML',23);   
insert into especialidade (id, nome, sigla, corpo_quadro_id) values (142, 'Meteorologia','ME',23);   
insert into especialidade (id, nome, sigla, corpo_quadro_id) values (143, 'Motores','MS',23);   
insert into especialidade (id, nome, sigla, corpo_quadro_id) values (144, 'Nutrição e Dietética','ND',23); 
insert into especialidade (id, nome, sigla, corpo_quadro_id) values (145, 'Paiol','PL',23);   
insert into especialidade (id, nome, sigla, corpo_quadro_id) values (146, 'Patologia Clínica','PC',23);  
insert into especialidade (id, nome, sigla, corpo_quadro_id) values (147, 'Processamento de Dados','PD',23); 
insert into especialidade (id, nome, sigla, corpo_quadro_id) values (148, 'Prótese Dentária','PT',23);  
insert into especialidade (id, nome, sigla, corpo_quadro_id) values (149, 'Química','QI',23);   
insert into especialidade (id, nome, sigla, corpo_quadro_id) values (150, 'Radiologia Médica','RM',23);  
insert into especialidade (id, nome, sigla, corpo_quadro_id) values (151, 'Reabilitação','RB',23);   
insert into especialidade (id, nome, sigla, corpo_quadro_id) values (152, 'Secretariado','SC',23);   
insert into especialidade (id, nome, sigla, corpo_quadro_id) values (153, 'Telecomunicações','TC',23);   


ALTER TABLE usuario ALTER COLUMN id SET DEFAULT nextval ('usuario_seq'::regclass);
insert into usuario(login, nip, senha, ativo, data_cadastro, nome_completo, nome_guerra, posto_graduacao_id, corpo_quadro_id)
values('admin', '12.3456.78', '$2a$06$qJJHrUnfyhIBXGq4IB.ZHO0heXmxUfN/qSFqCXZzK0OsLMqFx5CWu', 't', now(), 'Administrador', 'Administrador', 10, 11);

ALTER TABLE perfil ALTER COLUMN id SET DEFAULT nextval ('perfil_seq'::regclass);
insert into perfil(nome, descricao) values('PERFIL_CONVENCIONAL', 'Convencional');
insert into perfil(nome, descricao) values('PERFIL_ADMINISTRADOR', 'Administrador');

insert into usuario_perfil (usuario_id, perfil_id) values (1, 1);
insert into usuario_perfil (usuario_id, perfil_id) values (1, 2);

ALTER TABLE permissao ALTER COLUMN id SET DEFAULT nextval ('permissao_seq'::regclass);
insert into permissao(nome) values('LOGIN');
insert into permissao(nome) values('CADASTRAR_USUARIO');
insert into permissao(nome) values('CADASTRAR_OM');

insert into perfil_permissao (perfil_id, permissao_id) values (1, 1);
insert into perfil_permissao (perfil_id, permissao_id) values (2, 2);
insert into perfil_permissao (perfil_id, permissao_id) values (2, 3);
