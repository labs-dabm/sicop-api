package br.mil.marinha.dabm.sicop.service;

import java.util.List;

import br.mil.marinha.dabm.sicop.dominio.PostoGraduacao;

public interface PostoGraduacaoService
{

    public List<PostoGraduacao> getTodosPostosGraduacoes();

}
