package br.mil.marinha.dabm.sicop.repository;

import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import br.mil.marinha.dabm.sicop.dominio.Perfil;

@Repository
public interface PerfilRepository extends PagingAndSortingRepository<Perfil, Long>
{

}
