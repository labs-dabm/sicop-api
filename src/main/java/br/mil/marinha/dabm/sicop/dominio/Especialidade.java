package br.mil.marinha.dabm.sicop.dominio;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name = "especialidade")
public class Especialidade
{

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "especialidade_seq")
    @SequenceGenerator(name = "especialidade_seq", sequenceName = "especialidade_seq", allocationSize = 1, initialValue = 1)
    @Column(unique = true, nullable = false, updatable = false, insertable = true)
    private Long id;

    private String nome;
    private String sigla;

    @ManyToOne
    @JoinColumn(name = "corpo_quadro_id")
    private CorpoQuadro corpoQuadro;

    public Long getId()
    {
        return this.id;
    }

    public void setId(Long id)
    {
        this.id = id;
    }

    public String getNome()
    {
        return this.nome;
    }

    public void setNome(String nome)
    {
        this.nome = nome;
    }

    public String getSigla()
    {
        return this.sigla;
    }

    public void setSigla(String sigla)
    {
        this.sigla = sigla;
    }

    public CorpoQuadro getCorpoQuadro()
    {
        return corpoQuadro;
    }

    public void setCorpoQuadro(CorpoQuadro corpoQuadro)
    {
        this.corpoQuadro = corpoQuadro;
    }

    @Override
    public int hashCode()
    {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((nome == null) ? 0 : nome.hashCode());
        result = prime * result + ((sigla == null) ? 0 : sigla.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj)
    {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        Especialidade other = (Especialidade) obj;
        if (nome == null) {
            if (other.nome != null)
                return false;
        } else if (!nome.equals(other.nome))
            return false;
        if (sigla == null) {
            if (other.sigla != null)
                return false;
        } else if (!sigla.equals(other.sigla))
            return false;
        return true;
    }

}
