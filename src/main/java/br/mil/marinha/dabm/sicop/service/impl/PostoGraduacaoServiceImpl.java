package br.mil.marinha.dabm.sicop.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.mil.marinha.dabm.sicop.dominio.PostoGraduacao;
import br.mil.marinha.dabm.sicop.repository.PostoGraduacaoRepository;
import br.mil.marinha.dabm.sicop.service.PostoGraduacaoService;

@Service
public class PostoGraduacaoServiceImpl implements PostoGraduacaoService
{

    @Autowired
    private PostoGraduacaoRepository postoGraduacaoRepository;

    @Override
    public List<PostoGraduacao> getTodosPostosGraduacoes()
    {
        return (List<PostoGraduacao>) postoGraduacaoRepository.findAllByOrderByOrdemAsc();
    }

}
