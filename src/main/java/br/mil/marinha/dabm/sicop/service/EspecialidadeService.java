package br.mil.marinha.dabm.sicop.service;

import java.util.List;

import br.mil.marinha.dabm.sicop.dominio.CorpoQuadro;
import br.mil.marinha.dabm.sicop.dominio.Especialidade;

public interface EspecialidadeService
{

    public List<Especialidade> getTodasEspecialidades();

    public List<Especialidade> getTodasEspecialidadesPorCorpoQuadro(CorpoQuadro corpoQuadro);

    public List<Especialidade> getTodasEspecialidadesPorCorpoQuadro(Long corpoQuadroId);

}
