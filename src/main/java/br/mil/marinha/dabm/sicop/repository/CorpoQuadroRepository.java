package br.mil.marinha.dabm.sicop.repository;

import java.util.List;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import br.mil.marinha.dabm.sicop.dominio.CorpoQuadro;

@Repository
public interface CorpoQuadroRepository extends CrudRepository<CorpoQuadro, Long>
{

    public List<CorpoQuadro> findAllByOrderByOrdemAsc();

}
