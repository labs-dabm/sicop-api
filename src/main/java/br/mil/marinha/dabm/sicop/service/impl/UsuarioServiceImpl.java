package br.mil.marinha.dabm.sicop.service.impl;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import br.mil.marinha.dabm.sicop.dominio.Usuario;
import br.mil.marinha.dabm.sicop.repository.UsuarioRepository;
import br.mil.marinha.dabm.sicop.service.UsuarioService;

@Service
public class UsuarioServiceImpl implements UsuarioService
{

    @Autowired
    private UsuarioRepository usuarioRepository;

    @Override
    public Usuario getUsuarioPeloLogin(String login)
    {
        return usuarioRepository.findByLogin(login);
    }

    @Override
    public List<Usuario> getUsuarios()
    {
        return (List<Usuario>) usuarioRepository.findAll();
    }

    @Override
    public Page<Usuario> listarUsuariosPaginados(Pageable pageable)
    {
        return (Page<Usuario>) usuarioRepository.findAll(pageable);
    }

    @Override
    public Usuario salvar(Usuario usuario)
    {
        return usuarioRepository.save(usuario);
    }

    @Override
    public Usuario salvar(Long id, Usuario user)
    {

        Usuario usuarioSalvo = buscarPessoaPeloCodigo(id);

        BeanUtils.copyProperties(user, usuarioSalvo, "id");
        
        System.out.println(usuarioSalvo.getLogin());
        System.out.println(user.getLogin());

        return usuarioRepository.save(usuarioSalvo);
    }

    @Override
    public Optional<Usuario> buscarUsuarioPeloCodigo(Long codigo)
    {
        return usuarioRepository.findById(codigo);
    }

    @Override
    public void removerUsuarioPeloId(Long codigo)
    {
        usuarioRepository.deleteById(codigo);
    }
    
    public Usuario buscarPessoaPeloCodigo(Long codigo)
    {

        Usuario u = usuarioRepository.getOne(codigo);

        if (u == null) {
            System.out.println("vazio");
            throw new EmptyResultDataAccessException(1);
        } else {
            System.out.println(u.getLogin());
        }

        return u;
    }

}
