package br.mil.marinha.dabm.sicop.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.mil.marinha.dabm.sicop.dominio.CorpoQuadro;
import br.mil.marinha.dabm.sicop.repository.CorpoQuadroRepository;
import br.mil.marinha.dabm.sicop.service.CorpoQuadroService;

@Service
public class CorpoQuadroServiceImpl implements CorpoQuadroService
{

    @Autowired
    private CorpoQuadroRepository corpoQuadroRepository;

    @Override
    public List<CorpoQuadro> getTodosCorposQuadros()
    {
        return corpoQuadroRepository.findAllByOrderByOrdemAsc();
    }

}
