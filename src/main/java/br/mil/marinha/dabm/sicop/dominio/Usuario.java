package br.mil.marinha.dabm.sicop.dominio;

import java.io.Serializable;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import com.fasterxml.jackson.annotation.JsonBackReference;

@Entity
public class Usuario implements Serializable
{

    private static final long serialVersionUID = -6555338884914668806L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "usuario_seq")
    @SequenceGenerator(name = "usuario_seq", sequenceName = "usuario_seq", allocationSize = 1, initialValue = 1)
    @Column(unique = true, nullable = false, updatable = false, insertable = true)
    private Long id;

    @NotNull
    @Column(updatable = false)
    private String login;

    private String senha;

    @Transient
    private String confirmaSenha;

    @Column(name = "nome_completo")
    private String nomeCompleto;

    @NotNull //(message = "O Nome de Guerra é obrigatório.")
    @Column(name = "nome_guerra")
    private String nomeGuerra;

    @Column(unique = true, nullable = false, updatable = false, length = 10)
    @NotNull
    @Size(min = 10, max = 10, message = "O NIP deve conter 8 caracteres numéricos")
    private String nip;

    @ManyToOne
    @JoinColumn(name = "corpo_quadro_id")
    private CorpoQuadro corpoQuadro;

    @ManyToOne
    @JoinColumn(name = "posto_graduacao_id")
    private PostoGraduacao postoGraduacao;

    @ManyToOne
    @JoinColumn(name = "especialidade_id")
    private Especialidade especialidade;

    @Email(message = "No campo E-MAIL utilize o formato exemplo@exemplo.com")
    private String email;
    private Boolean ativo;

    @Column(name = "data_cadastro")
    @Temporal(TemporalType.TIMESTAMP)
    private Date dataCadastro;

    @ManyToMany(fetch = FetchType.EAGER)
    @JoinTable(name = "usuario_perfil", joinColumns = {
            @JoinColumn(name = "usuario_id", referencedColumnName = "id") }, inverseJoinColumns = {
                    @JoinColumn(name = "perfil_id", referencedColumnName = "id", unique = false) })
    @JsonBackReference
    private Set<Perfil> perfis;

    @ManyToOne
    @JoinColumn(name = "organizacao_militar_id")
    private OrganizacaoMilitar organizacaoMilitar;

    public Usuario()
    {
        ativo = true;
        dataCadastro = new Date();
        perfis = new HashSet<Perfil>();
    }

    public Long getId()
    {
        return id;
    }

    public void setId(Long id)
    {
        this.id = id;
    }

    public String getLogin()
    {
        return login;
    }

    public void setLogin(String login)
    {
        this.login = login;
    }

    public String getSenha()
    {
        return senha;
    }

    public void setSenha(String senha)
    {
        this.senha = senha;
    }

    public String getConfirmaSenha()
    {
        return confirmaSenha;
    }

    public void setConfirmaSenha(String confirmaSenha)
    {
        this.confirmaSenha = confirmaSenha;
    }

    public Boolean isAtivo()
    {
        return ativo;
    }

    public void setAtivo(Boolean ativo)
    {
        this.ativo = ativo;
    }

    public Date getDataCadastro()
    {
        return dataCadastro;
    }

    public void setDataCadastro(Date dataCadastro)
    {
        this.dataCadastro = dataCadastro;
    }

    public Set<Perfil> getPerfis()
    {
        return perfis;
    }

    public void setPerfis(Set<Perfil> perfis)
    {
        this.perfis = perfis;
    }

    public String getNomeCompleto()
    {
        return nomeCompleto;
    }

    public void setNomeCompleto(String nomeCompleto)
    {
        this.nomeCompleto = nomeCompleto;
    }

    public String getNomeGuerra()
    {
        return nomeGuerra;
    }

    public void setNomeGuerra(String nomeGuerra)
    {
        this.nomeGuerra = nomeGuerra;
    }

    public String getNip()
    {
        return nip;
    }

    public void setNip(String nip)
    {
        this.nip = nip;
    }

    public CorpoQuadro getCorpoQuadro()
    {
        return corpoQuadro;
    }

    public void setCorpoQuadro(CorpoQuadro corpoQuadro)
    {
        this.corpoQuadro = corpoQuadro;
    }

    public PostoGraduacao getPostoGraduacao()
    {
        return postoGraduacao;
    }

    public void setPostoGraduacao(PostoGraduacao postoGraduacao)
    {
        this.postoGraduacao = postoGraduacao;
    }

    public Especialidade getEspecialidade()
    {
        return especialidade;
    }

    public void setEspecialidade(Especialidade especialidade)
    {
        this.especialidade = especialidade;
    }

    public String getEmail()
    {
        return email;
    }

    public void setEmail(String email)
    {
        this.email = email;
    }

    public OrganizacaoMilitar getOrganizacaoMilitar()
    {
        return organizacaoMilitar;
    }

    public void setOrganizacaoMilitar(OrganizacaoMilitar organizacaoMilitar)
    {
        this.organizacaoMilitar = organizacaoMilitar;
    }

    @Override
    public int hashCode()
    {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((login == null) ? 0 : login.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj)
    {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        Usuario other = (Usuario) obj;
        if (login == null) {
            if (other.login != null)
                return false;
        } else if (!login.equals(other.login))
            return false;
        return true;
    }

}
