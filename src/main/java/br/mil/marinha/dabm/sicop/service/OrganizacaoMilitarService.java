package br.mil.marinha.dabm.sicop.service;

import java.util.List;

import org.springframework.data.domain.Pageable;

import br.mil.marinha.dabm.sicop.dominio.OrganizacaoMilitar;

public interface OrganizacaoMilitarService
{

    public List<OrganizacaoMilitar> listarOrganizacoesMilitaresPaginadas(Pageable pageable);

    public OrganizacaoMilitar obterOrganizacaoMilitarPeloId(Long organizacaoMilitarId);

    public List<OrganizacaoMilitar> listarTodasOrganizacoesMilitares();

}
