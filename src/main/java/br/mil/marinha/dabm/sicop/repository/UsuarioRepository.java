package br.mil.marinha.dabm.sicop.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.PagingAndSortingRepository;

import br.mil.marinha.dabm.sicop.dominio.Usuario;

public interface UsuarioRepository extends PagingAndSortingRepository<Usuario, Long>, JpaRepository<Usuario, Long>
{

    public Usuario findByLogin(String login);

    public Usuario findByNip(String nip);

}
