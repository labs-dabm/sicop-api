package br.mil.marinha.dabm.sicop.repository;

import java.util.List;
import java.util.Optional;

import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import br.mil.marinha.dabm.sicop.dominio.OrganizacaoMilitar;

@Repository
public interface OrganizacaoMilitarRepository extends PagingAndSortingRepository<OrganizacaoMilitar, Long>
{

    public List<OrganizacaoMilitar> findAllByOrderByNomeAsc();

    public List<OrganizacaoMilitar> findByNomeIgnoreCaseContaining(String parteNome);

    public Optional<OrganizacaoMilitar> findByNome(String nome);

}
