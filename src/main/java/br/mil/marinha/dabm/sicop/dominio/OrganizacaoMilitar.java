package br.mil.marinha.dabm.sicop.dominio;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;

@Entity
@Table(name = "organizacao_militar")
public class OrganizacaoMilitar
{

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "organizacao_militar_seq")
    @SequenceGenerator(name = "organizacao_militar_seq", sequenceName = "organizacao_militar_seq", allocationSize = 1, initialValue = 1)
    @Column(unique = true, nullable = false, updatable = false, insertable = true)
    private Long id;

    @NotBlank(message = "O campo NOME é obrigatório")
    @Column(unique = true)
    private String nome;

    @NotBlank(message = "O campo SIGLA é obrigatório")
    @Column(unique = true)
    private String sigla;

    @NotBlank(message = "O campo CÓDIGO é obrigatório")
    @Size(min = 6, max = 6, message = "O campo CÓDIGO deve conter 6 caracteres")
    private String codigo;

    @Size(min = 7, max = 7, message = "O campo INDICATIVO NAVAL deve conter 7 caracteres")
    @Column(name = "indicativo_naval")
    private String indicativoNaval;
    private Boolean ativa;

    public OrganizacaoMilitar()
    {
        this.ativa = true;
    }

    public Long getId()
    {
        return id;
    }

    public void setId(Long id)
    {
        this.id = id;
    }

    public String getNome()
    {
        return nome;
    }

    public void setNome(String nome)
    {
        this.nome = nome;
    }

    public String getSigla()
    {
        return sigla;
    }

    public void setSigla(String sigla)
    {
        this.sigla = sigla;
    }

    public String getCodigo()
    {
        return codigo;
    }

    public void setCodigo(String codigo)
    {
        this.codigo = codigo;
    }

    public String getIndicativoNaval()
    {
        return indicativoNaval;
    }

    public void setIndicativoNaval(String indicativoNaval)
    {
        this.indicativoNaval = indicativoNaval;
    }

    public Boolean getAtiva()
    {
        return ativa;
    }

    public void setAtiva(Boolean ativa)
    {
        this.ativa = ativa;
    }

    @Override
    public String toString()
    {
        return this.nome;
    }

}
