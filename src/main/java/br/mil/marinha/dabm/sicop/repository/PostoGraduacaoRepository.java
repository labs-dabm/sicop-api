package br.mil.marinha.dabm.sicop.repository;

import java.util.List;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import br.mil.marinha.dabm.sicop.dominio.PostoGraduacao;

@Repository
public interface PostoGraduacaoRepository extends CrudRepository<PostoGraduacao, Long>
{

    public List<PostoGraduacao> findAllByOrderByOrdemAsc();

}
