package br.mil.marinha.dabm.sicop.service.impl;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.mil.marinha.dabm.sicop.dominio.CorpoQuadro;
import br.mil.marinha.dabm.sicop.dominio.Especialidade;
import br.mil.marinha.dabm.sicop.repository.CorpoQuadroRepository;
import br.mil.marinha.dabm.sicop.repository.EspecialidadeRepository;
import br.mil.marinha.dabm.sicop.service.EspecialidadeService;

@Service
public class EspecialidadeServiceImpl implements EspecialidadeService
{

    @Autowired
    private EspecialidadeRepository especialidadeRepository;

    @Autowired
    private CorpoQuadroRepository corpoQuadroRepository;

    @Override
    public List<Especialidade> getTodasEspecialidades()
    {
        return especialidadeRepository.findAllByOrderByNomeAsc();
    }

    @Override
    public List<Especialidade> getTodasEspecialidadesPorCorpoQuadro(CorpoQuadro corpoQuadro)
    {
        return especialidadeRepository.findByCorpoQuadro(corpoQuadro);
    }

    @Override
    public List<Especialidade> getTodasEspecialidadesPorCorpoQuadro(Long corpoQuadroId)
    {
        Optional<CorpoQuadro> corpoQuadro = corpoQuadroRepository.findById(corpoQuadroId);
        return especialidadeRepository.findByCorpoQuadro(corpoQuadro.orElseGet(CorpoQuadro::new));
    }

}
