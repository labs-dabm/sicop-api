package br.mil.marinha.dabm.sicop;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SicopApplication
{

    public static void main(String[] args)
    {
        SpringApplication.run(SicopApplication.class, args);
    }

}
