package br.mil.marinha.dabm.sicop.repository;

import java.util.List;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import br.mil.marinha.dabm.sicop.dominio.CorpoQuadro;
import br.mil.marinha.dabm.sicop.dominio.Especialidade;

@Repository
public interface EspecialidadeRepository extends CrudRepository<Especialidade, Long>
{

    public List<Especialidade> findAllByOrderByNomeAsc();

    public List<Especialidade> findByCorpoQuadro(CorpoQuadro corpoQuadro);

}
