package br.mil.marinha.dabm.sicop.dominio;

import java.io.Serializable;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.SequenceGenerator;

@Entity
public class Permissao implements Serializable
{

    private static final long serialVersionUID = 7260113665437966520L;

    @Id
    @Column(unique = true, nullable = false, updatable = false, insertable = true)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "permissao_seq")
    @SequenceGenerator(name = "permissao_seq", sequenceName = "permissao_seq", allocationSize = 1, initialValue = 1)
    private Long id;
    private String nome;

    @ManyToMany(mappedBy = "permissoes")
    private Set<Perfil> perfis;

    public Long getId()
    {
        return id;
    }

    public void setId(Long id)
    {
        this.id = id;
    }

    public String getNome()
    {
        return nome;
    }

    public void setNome(String nome)
    {
        this.nome = nome;
    }

    public Set<Perfil> getPerfis()
    {
        return perfis;
    }

    public void setPerfis(Set<Perfil> perfis)
    {
        this.perfis = perfis;
    }

}
