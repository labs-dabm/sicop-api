package br.mil.marinha.dabm.sicop.dominio;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name = "perfil")
public class Perfil implements Serializable
{

    private static final long serialVersionUID = -879866884083586539L;

    @Id
    @Column(unique = true, nullable = false, updatable = false, insertable = true)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "perfil_seq")
    @SequenceGenerator(name = "perfil_seq", sequenceName = "perfil_seq", allocationSize = 1, initialValue = 1)
    private Long id;

    @Column(unique = true, nullable = false, insertable = true)
    private String nome;
    private String descricao;

    @ManyToMany(mappedBy = "perfis")
    private Set<Usuario> usuarios;

    @ManyToMany(fetch = FetchType.EAGER)
    @JoinTable(name = "perfil_permissao", joinColumns = {
            @JoinColumn(name = "perfil_id", referencedColumnName = "id") }, inverseJoinColumns = {
                    @JoinColumn(name = "permissao_id", referencedColumnName = "id", unique = false) })
    private Set<Permissao> permissoes;

    // get/set
    public Perfil()
    {
        this.permissoes = new HashSet<Permissao>();
    }

    public Long getId()
    {
        return id;
    }

    public void setId(Long id)
    {
        this.id = id;
    }

    public String getNome()
    {
        return nome;
    }

    public void setNome(String nome)
    {
        this.nome = nome;
    }

    @Column(nullable = false, insertable = true)
    public String getDescricao()
    {
        return descricao;
    }

    public void setDescricao(String descricao)
    {
        this.descricao = descricao;
    }

    public Set<Permissao> getPermissoes()
    {
        return permissoes;
    }

    public void setPermissoes(Set<Permissao> permissoes)
    {
        this.permissoes = permissoes;
    }

    public Set<Usuario> getUsuarios()
    {
        return usuarios;
    }

    public void setUsuarios(Set<Usuario> usuarios)
    {
        this.usuarios = usuarios;
    }
}
