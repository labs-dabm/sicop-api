package br.mil.marinha.dabm.sicop.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import br.mil.marinha.dabm.sicop.dominio.OrganizacaoMilitar;
import br.mil.marinha.dabm.sicop.repository.OrganizacaoMilitarRepository;
import br.mil.marinha.dabm.sicop.service.OrganizacaoMilitarService;

@Service
public class OrganizacaoMilitarServiceImpl implements OrganizacaoMilitarService
{

    @Autowired
    private OrganizacaoMilitarRepository organizacaoMilitarRepository;

    @Override
    public List<OrganizacaoMilitar> listarOrganizacoesMilitaresPaginadas(Pageable pageable)
    {
        return organizacaoMilitarRepository.findAll(pageable).getContent();
    }

    @Override
    public OrganizacaoMilitar obterOrganizacaoMilitarPeloId(Long organizacaoMilitarId)
    {
        return organizacaoMilitarRepository.findById(organizacaoMilitarId).orElse(null);
    }

    @Override
    public List<OrganizacaoMilitar> listarTodasOrganizacoesMilitares()
    {
        return organizacaoMilitarRepository.findAllByOrderByNomeAsc();
    }

}
