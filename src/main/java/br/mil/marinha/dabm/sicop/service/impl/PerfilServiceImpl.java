package br.mil.marinha.dabm.sicop.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.mil.marinha.dabm.sicop.dominio.Perfil;
import br.mil.marinha.dabm.sicop.repository.PerfilRepository;
import br.mil.marinha.dabm.sicop.service.PerfilService;

@Service
public class PerfilServiceImpl implements PerfilService
{

    @Autowired
    private PerfilRepository perfilRepository;

    @Override
    public List<Perfil> getTodosPerfis()
    {
        return (List<Perfil>) perfilRepository.findAll();
    }

}
