package br.mil.marinha.dabm.sicop.service;

import java.util.List;

import br.mil.marinha.dabm.sicop.dominio.CorpoQuadro;

public interface CorpoQuadroService
{

    public List<CorpoQuadro> getTodosCorposQuadros();

}
