package br.mil.marinha.dabm.sicop.dominio;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name = "posto_graduacao")
public class PostoGraduacao
{

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "posto_graduacao_seq")
    @SequenceGenerator(name = "posto_graduacao_seq", sequenceName = "posto_graduacao_seq", allocationSize = 1, initialValue = 1)
    @Column(unique = true, nullable = false, updatable = false, insertable = true)
    private Long id;
    private String nome;
    private String sigla;
    private Integer ordem;

    public Long getId()
    {
	return id;
    }

    public void setId(Long id)
    {
	this.id = id;
    }

    public String getNome()
    {
	return nome;
    }

    public void setNome(String nome)
    {
	this.nome = nome;
    }

    public String getSigla()
    {
	return sigla;
    }

    public void setSigla(String sigla)
    {
	this.sigla = sigla;
    }

    public Integer getOrdem()
    {
	return ordem;
    }

    public void setOrdem(Integer ordem)
    {
	this.ordem = ordem;
    }

    @Override
    public String toString()
    {
	return sigla;
    }

}
