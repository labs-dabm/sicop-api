package br.mil.marinha.dabm.sicop.service;

import java.util.List;

import br.mil.marinha.dabm.sicop.dominio.Perfil;

public interface PerfilService
{

    public List<Perfil> getTodosPerfis();

}
