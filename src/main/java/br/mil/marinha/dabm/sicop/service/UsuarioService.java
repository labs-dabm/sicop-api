package br.mil.marinha.dabm.sicop.service;

import java.util.List;
import java.util.Optional;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import br.mil.marinha.dabm.sicop.dominio.Usuario;

public interface UsuarioService
{

    public Usuario getUsuarioPeloLogin(String login);

    public List<Usuario> getUsuarios();

    public Usuario salvar(Usuario usuario);
    
    public Usuario salvar(Long codigo, Usuario pessoa);

    public Optional<Usuario> buscarUsuarioPeloCodigo(Long codigo);

    public Page<Usuario> listarUsuariosPaginados(Pageable pageable);
    
    public void removerUsuarioPeloId(Long codigo);        

}
